# entraîner sur des superpositions ie des spectres mélangés. AUssi checker le CNN, plus adapté à l'analyse de spectres

#une fois le modèle entrainé utiliser model.predict()

from sklearn.metrics import accuracy_score
import numpy as np
import random
import torch
import torch.nn as nn
import torch.optim as optim
from sklearn.metrics import f1_score
from sklearn.model_selection import train_test_split
import random
import pickle
import os
import copy


rd_chosen = 7


# Créations de données de test
# Il y a 2^8 différents types d'entrée car il y a 256 combinaisons de 8 capteurs

path_to_fft_etiquete = "./train_aug"

num_spectre = 3000
nb_iteration = 1000

# Définir la taille cible des spectres réduits
target_size = 1000

# #va contenir les spectres étiquetés pour 8 capyeurs 
# spectres_fft = {}
# for i in range(1,2):
#     spectres_fft[format(i, f'0{8}b')] = []

#va contenir les spectres étiquetés pour 2 capteurs à la fois
spectres_fft = {}
# Initialisation du dictionnaire
binary_dict = {}

# Parcourir toutes les positions possibles des "1"
for i in range(8):
    for j in range(i + 1, 8):
        # Créer la clé binaire avec deux "1" à la position i et j
        binary_key = "0" * i + "1" + "0" * (j - i - 1) + "1" + "0" * (8 - j - 1)
        
        # Ajouter la clé au dictionnaire avec une valeur par défaut (ici, None)
        spectres_fft[binary_key] = []

#chargement des données d'entrée des csv, ie des spectres fft étiquetés :
l = os.listdir(path_to_fft_etiquete)

def fft_mat(path):
    with open(path_to_fft_etiquete+"/"+path, 'rb') as fichier:
        l = pickle.load(fichier)
    if len(l[1]) < num_spectre :
        dif = [0]*(num_spectre-len(l[1]))  #filtrage des listes vides
        help = copy.deepcopy(l[1])
        help_2 = copy.deepcopy(l[0])
        help += dif
        help_2 += dif
        matrice = np.array([help_2, help])
    else :
        matrice = np.array([l[0], l[1]])    #ligne 1 = fréquence et ligne 2 = amplitude
    return matrice


for fich in l :    #on veut fich de la forme fich ="10010010_00.txt"
    label = fich[:8]
    spectre = fft_mat(fich)[1]     #matrice de dim 2
    # Réduire la taille des spectres en utilisant une interpolation linéaire
    spectres_fft[label].append(spectre)
# print('ceci est spectre 00101111', spectres_fft['00101111'][0][:5])

## Données étiquetées

# Définir les étiquettes de classe : représentation binaire
"""une étiquette est représentée par 8 digits 1 ou 0 : 1 si le capteurs est présent et 0 sinon
exemple : etiquette = 00001001 les capteurs  1 et 4 sont présents
!!! à changer dans les étiquettes des spectres car on a fait une représentation inverse avec François !!!
c'est à dire que le 1 est le premier digit donc l'exemple précédent aurait été : 10010000"""

#label pour 256 combinaisons ie pour 8 capteurs qui peuvent être à la fois
#labels_binaire = [format(i, f'0{8}b') for i in range(1,256)]     #permet de mieux lire à la fin quel capteurs sont présents

#label pour 28 combinaisons on ne garde que 2 capteurs par spectres
labels_binaire = []

for i in range(8):  # Parcourir les 8 positions possibles pour le premier 1
    for j in range(i + 1, 8):  # Parcourir les positions restantes pour le deuxième 1
        binary_number = (1 << i) | (1 << j)  # Combinaison binaire en utilisant des opérations binaires
        labels_binaire.append(format(binary_number, '08b'))  # Ajouter le nombre binaire à la liste

# Créer un dictionnaire de correspondance des labels
label_to_index = {label: index for index, label in enumerate(labels_binaire)}

# data sous forme de liste de couples (timestamps, label). timestamp est une liste de 5 temps
data =[]
for key in spectres_fft.keys():
    data.append((spectres_fft[key],key))      #liste de couple (spectre, étiquette) ex (spectres,'10001011')

# Séparer les données en ensembles d'entraînement et de test
X = []
y = []
for example in data:
    spectres = example[0]
    label = example[1]
    for spectre in spectres:
        if len(spectre) > 0 :
            print('le spectre et label sont :',spectre[:4],label)
            X.append(spectre)
            y.append(label)


# Mélanger les données pour s'assurer d'avoir de tout dans l'entraînement et dans le test
data = list(zip(X, y))  # Regrouper les entrées et les étiquettes ensemble
random.shuffle(data)  # Mélanger les données de manière aléatoire

# Séparer les données en ensembles d'entraînement et de test
X_shuffled, y_shuffled = zip(*data)  # Séparer les entrées et les étiquettes
X_train, X_test, y_train, y_test = train_test_split(
    X_shuffled,
    y_shuffled,
    test_size=0.2,
    random_state= 810  # 80% entraînement, 20% test. Choix de manière aléatoire
)


#print("X_train est :",X_train[:1])

# Afficher la taille des ensembles d'entraînement et de test
# print("Taille de l'ensemble d'entraînement :", len(X_train))
# print("Taille de l'ensemble de test :", len(X_test))


# Définir l'architecture du réseau de neurones convolutionnels (CNN)
class Net(nn.Module):  # résoudre des problèmes de classification binaire
    def __init__(self):
        super(Net, self).__init__()
        self.fc1 = nn.Linear(num_spectre, num_spectre)  # Couche 1 : Entrée 1000, nb points d'un spectre. sortie 100
        self.fc2 = nn.Linear(num_spectre, 500)  # Couche 2 : Entrée 100, sortie 100
        self.dropout1 = nn.Dropout(0.5)
        self.fc3 = nn.Linear(500, 100)
        self.dropout2 = nn.Dropout(0.3)
        self.fc4 = nn.Linear(100, 28
        )  # Couche 3 : Entrée 100, sortie 28 pour une classification à 28 valeurs

#Définit le flux de données à travers les couches du réseau
    def forward(self, x):
        x = torch.relu(self.fc1(x))
        x = torch.relu(self.fc2(x))
        x = self.dropout1(x)
        x = torch.relu(self.fc3(x))
        x = self.dropout2(x)
        x = self.fc4(x)
        return x



# Instancier le modèle
model = Net()

# Définir la fonction de perte et l'optimiseur
criterion = nn.CrossEntropyLoss()  # fonction de clustering
optimizer = optim.Adagrad(model.parameters(), lr=0.001, weight_decay=0.001)  # Stochastic Gradient Descent

"""lr est le taux d'apprentissage : si la méthode converge trop lentement il faut augmenter lr 
et si elle diverge ou est instable il faut diminué lr"""

# L'optimiseur est initialisé avec les paramètres du modèle
# afin de connaître les poids qui doivent être mis à jour. Le  lr (learning rate)
#  contrôle la taille des pas de mise à jour effectués par l'optimiseur.


# Préparer les données d'entrée et de sortie
X_train = np.array(X_train)
X_train = torch.tensor(X_train, dtype=torch.float32)
y_train = torch.tensor([label_to_index[label] for label in y_train], dtype=torch.long)
#print(y_train)


# Entraîner le réseau de neurones
for epoch in range(nb_iteration):  #chaque "epoch" correspond à une passe complète à travers l'ensemble d'entraînement
    print(epoch*100/nb_iteration,"%")
    optimizer.zero_grad() #remettre à zéros pour ne pas accumuler les gradients des itérations précédentes
    outputs = model(X_train)
    loss = criterion(outputs, y_train)
    loss.backward()
    optimizer.step()  #mise à jour des poinds du modèle

# Faire des prédictions sur l'ensemble de test
X_test = torch.tensor(X_test, dtype=torch.float32)
outputs = model(X_test)
predicted_labels = torch.argmax(outputs, dim=1).detach().numpy()
# Convertir les prédictions en labels
predicted_labels = [labels_binaire[p] for p in predicted_labels]

print(predicted_labels)

# Calculer le F1-score
f1 = f1_score(
    [labels_binaire.index(label) for label in y_test],
    [labels_binaire.index(p) for p in predicted_labels],
    average="weighted",
)


# Calculer l'exactitude (accuracy) sur l'ensemble de test
accuracy = accuracy_score( 
    [labels_binaire.index(label) for label in y_test],
    [labels_binaire.index(p) for p in predicted_labels],
)


# Pour ces 2 facteurs, une valeur proche de 1 est souhaitable mais il est normal qu'on en soit loin

# Afficher l'exactitude (accuracy) sur l'ensemble de test
print("Exactitude (accuracy) sur l'ensemble de test :", accuracy)

# Afficher le F1-score du modèle sur l'ensemble de test
print("F1-score sur l'ensemble de test :", f1)
