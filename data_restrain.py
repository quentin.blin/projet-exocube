## Cette fonction à pour but de résuire la taille des spectres en ne conservant que les infos importantes, à savoir les pics d'amplitude pour limiter la taille des données

import pickle
import os


def data_limit(path_to_pkl, dirsave_path): # limiter la taille des données (passer de plusieurs millions de points à quelques milliers)
    name = path_to_pkl.split('/')[-1]
    with open(path_to_pkl, 'rb') as fichier:
        l = pickle.load(fichier)
    rest_ampl = []
    freqs = []
    for i in range(len(l[1])):
        if float(l[1][i]) >= 300000: # On fixe un seuil au delà duquel on conserve les amplitudes et fréquences 
            rest_ampl.append(float(l[1][i]))
            freqs.append(float(l[0][i]))
    l1 = [freqs, rest_ampl]
    f = open(dirsave_path + '/' + name , "wb")
    pickle.dump(l1, f) # enregistre le tout au format pickle

# ne décommenter que la ligne d'intérêt
fichiers = os.listdir('C:/Users/blinq/Documents/projet-exocube/FFT')
# fichiers = os.listdir('C:/Users/blinq/Documents/projet-exocube/FFT_aug')
# fichiers = os.listdir('C:/Users/blinq/Documents/projet-exocube/FFT_aug2')

for i in range(len(fichiers)):
    # # ne décommenter que la ligne d'intérêt
    data_limit('C:/Users/blinq/Documents/projet-exocube/FFT/'+fichiers[i], './train') # enregistrer les fichiers réduits dans train_aug2 dans cet exemple
    # data_limit('C:/Users/blinq/Documents/projet-exocube/FFT_aug/'+fichiers[i], './train_aug') # enregistrer les fichiers réduits dans train_aug2 dans cet exemple
    # data_limit('C:/Users/blinq/Documents/projet-exocube/FFT_aug2/'+fichiers[i], './train_aug2') # enregistrer les fichiers réduits dans train_aug2 dans cet exemple

    # with open('./train_aug2/' + fichiers[i], 'rb') as fichier:
    #     l = pickle.load(fichier)
    # print(len(l[0])) # print le nombre de points qui ont été gardés pour se rendre compte de la diminution importante du nombre de points 