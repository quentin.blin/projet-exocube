'''
Ce fichier construit une base de données à partir d'un fichier pcap en extrayant les paquets HTTP de la capture. Il utilise plusieurs fonctions pour extraire les informations principales de chaque paquet, fusionner les sessions, calculer les statistiques des sessions HTTP, et construire la base de données Pandas. La base de données finale ne contient pas la colonne 'SensorID'.'''

from Builder.helpers_pcap import *
from Builder.helpers_extractor import *
from Builder.helpers_db import *


def build_guess_db(path):
    """
    Cette fonction prend en entrée un chemin de fichier .pcap, extrait les paquets HTTP de la capture, et construit une base de données Pandas contenant les sessions HTTP extraites, sans la colonne 'SensorID'
    """
    # Charger la capture
    packets = loadPcap(path)
    # Extraire les paquets HTTP
    httpPackets = filterHTTPPackets(packets)
    # Créer un dictionnaire des balises associées à chaque capteur à partir de sensors_tags.txt
    tagsDictionnary = createTagsDictionnary()
    # Extraire les informations principales de chaque paquet HTTP
    packetsData = packetsDataExtractor(httpPackets, tagsDictionnary)
    # Fusionner les sessions "A > B" et "B > A" en "A <> B"
    packetsDataMerged = mergeUpDownSessions(packetsData)
    # Calculer les statistiques des sessions HTTP
    sessionsData = sessionsDataExtractor(packetsDataMerged)

    # Lignes de code qui n'est applicable qu'à un exemple de l'année dernière, à revoir
    sessionsData = sessionsData.loc[sessionsData['SensorID'].isin(['4.0', '6.0']), :] # permet de ne garder que les lignes pour lesquelles la colonne SensorID contient les valeurs '4.0' ou '6.0'
    sessionsData['SensorID'] = '?'

    # Construire la base de données
    x_guess = buildDataBase(sessionsData)
    # Supprimer la colonne 'SensorID'
    x_guess = x_guess.loc[:, x_guess.columns != 'SensorID'] # Le premier argument (:) indique que l'on souhaite sélectionner toutes les lignes, et le deuxième argument (x_guess.columns != 'SensorID') est une condition booléenne qui renvoie True pour toutes les colonnes sauf la colonne 'SensorID'

    return(x_guess)
