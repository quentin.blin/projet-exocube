'''Ce fichier utilise deux modèles de classification (XGBoost et RandomForest) pour prédire l'appareil qui a généré des paquets à partir d'une capture fournie. Il crée une base de données à partir de la capture, effectue les prédictions à l'aide des modèles et renvoie un tuple contenant les prédictions de chaque modèle avec leur précision respective.'''

from Guesser.helpers_guess import *
from Learner.learn import *

# Créer la base de données à partir d'une capture, en utilisant des cases


def findName(sensor_id, tagsDictionnary):
    """
    Cherche le nom associé à un ID de capteur dans un dictionnaire de balises.
    """
    for name, id in tagsDictionnary.keys():
        if id == sensor_id:
            return name


def guess(capture_path):
    """
    Utilise la capture fournie pour prédire l'appareil qui a généré les paquets en utilisant deux modèles de classification. Prend en entrée le chemin vers la capture, et renvoie un tuple contenant les prédictions de chaque modèle avec leur précision respective.
    """
    # Créer un dictionnaire de balises à partir du fichier 'sensors_tags.txt'
    tagsDictionnary = createTagsDictionnary()

    # Créer la base de données à partir de la capture fournie
    x_guess = build_guess_db(capture_path)

    # Prédire l'appareil qui a généré les paquets en utilisant les modèles de classification XGBoost et RandomForest
    _, _, prediction_xgboost, proba_xgboost = learn('xgboost', guess=True, x_guess=x_guess)
    _, _, prediction_randomforest, proba_randomforest = learn('xgboost', guess=True, x_guess=x_guess)

    # Créer des chaînes de caractères décrivant les prédictions de chaque modèle avec leur précision respective
    txt_xgboost = f"Prédiction de XGBoost : {findName(prediction_xgboost, tagsDictionnary)} (précision : {int(100*max(proba_xgboost[0]))}%)" # les fonctions de prédiction renvoient des probabilités pour chaque classe possible, donc en prenant le maximum de ces probabilités, on obtient la probabilité de la classe prédite
    txt_randomforest = f"Prédiction de RandomForest : {findName(prediction_randomforest, tagsDictionnary)} (précision : {int(100*max(proba_randomforest[0]))}%)"

    # Retourner les chaînes de caractères décrivant les prédictions de chaque modèle avec leur précision respective sous forme de tuple
    return (txt_xgboost, txt_randomforest)

