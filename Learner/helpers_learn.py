'''Ce fichier contient des fonctions pour le prétraitement des données, l'encodage des caractéristiques et des étiquettes, la construction de modèles de classification XGBoost et RandomForest, ainsi que l'évaluation des résultats. Les fonctions incluent l'importation d'un fichier CSV, la division des données en ensembles d'entraînement et de test, l'encodage des caractéristiques et des étiquettes, l'entraînement des modèles, et le calcul de la précision et de la matrice de confusion des prédictions.'''

# Data processing
from sklearn.model_selection import train_test_split
from sklearn.preprocessing import StandardScaler, LabelEncoder
import pandas as pd
import numpy as np
# Machine Learning Algorithms
from xgboost import XGBClassifier
from sklearn.ensemble import RandomForestClassifier
# Resultats
from sklearn.metrics import accuracy_score, confusion_matrix


### DATA PROCESSING ###

def import_csv(path):
    """
    Cette fonction lit un fichier CSV à partir d'un chemin d'accès et retourne sa conversion en dataframe pandas.
    """
    return(pd.read_csv(path))


def split(df, trainPercentage):
    """
    Cette fonction divise un dataframe en ensembles d'entraînement et de test, en fonction d'un pourcentage donné pour l'ensemble d'entraînement.

    Input :
    - df : un dataframe pandas contenant les données à diviser.
    - trainPercentage : un flottant compris entre 0 et 1, indiquant la proportion des données à utiliser pour l'ensemble d'entraînement.

    Output :
    - x_train : un dataframe contenant les caractéristiques de l'ensemble d'entraînement.
    - y_train : un dataframe contenant les étiquettes de l'ensemble d'entraînement.
    - x_test : un dataframe contenant les caractéristiques de l'ensemble de test.
    - y_test : un dataframe contenant les étiquettes de l'ensemble de test.
    """
    # Extraction des caractéristiques du dataframe
    features = df.loc[:, df.columns != 'SensorID']

    # Extraction des étiquettes du dataframe
    target = df['SensorID']

    # Division des données en ensembles d'entraînement et de test
    x_train, x_test, y_train, y_test = train_test_split(
        features, target, train_size=trainPercentage, random_state=0) # "random_state" est utilisé pour initialiser le générateur de nombres aléatoires, ce qui permet de reproduire les mêmes résultats chaque fois que la fonction est appelée avec les mêmes arguments
    return(x_train, y_train, x_test, y_test)


### ENCODAGE ###

def features_encoder(x_train, x_test, guess=False, x_guess=None):
    """
    Cette fonction encode les variables de caractéristiques en utilisant la méthode de la Standardisation et retourne les variables encodées pour l'ensemble d'entraînement et de test.
    """
    # Création d'un objet StandardScaler
    sc = StandardScaler()
    # Encodage des variables de caractéristiques pour l'ensemble d'entraînement
    x_train_encoded = sc.fit_transform(x_train) # Calcule la moyenne et l'écart-type des colonnes puis les utilise pour standardiser les données en les centrant autour de 0 et en les mettant à l'échelle
    # Encodage des variables de caractéristiques pour l'ensemble de test
    x_test_encoded = sc.transform(x_test) # applique la même transformation que pour l'ensemble d'entraînement de manière à conserver la même échelle et les mêmes propriétés que les données d'entraînement

    # Si le paramètre guess est True, encodage des variables de caractéristiques pour l'ensemble de prédiction (à fournir en argument)
    if guess:
        x_guess_encoded = sc.transform(x_guess)
    else:
        x_guess_encoded = None

    # Retour des variables encodées pour l'ensemble d'entraînement, de test et de prédiction
    return(x_train_encoded, x_test_encoded, x_guess_encoded)


def label_encoder(y_train, y_test):
    """
    Cette fonction encode les labels des ensembles d'entraînement et de test à l'aide de la méthode LabelEncoder.
    """
    # Initialisation de l'encodeur de labels
    le = LabelEncoder()
    # Concaténation des labels de l'ensemble d'entraînement et de test, et sélection des valeurs uniques
    values = pd.concat([y_train, y_test]).unique()
    # Apprentissage de l'encodeur sur les valeurs uniques des labels
    le.fit(values) # fit prend une liste d'étiquettes de classe en entrée et ajuste l'encodeur de manière à ce qu'il attribue une valeur numérique unique à chaque étiquette de classe
    # Renvoi des labels encodés
    return(le.transform(y_train), le.transform(y_test)) # transform prend une liste d'étiquettes de classe en entrée et renvoie une liste correspondante d'entiers qui ont été encodés


def label_decoder(y_train, y_test, y_guess_encoded):
    # Création d'un objet LabelEncoder pour encoder les étiquettes de classe
    le = LabelEncoder()
    # Concaténation des étiquettes de classe d'entraînement et de test pour encoder les étiquettes de classe de devinette
    values = pd.concat([y_train, y_test]).unique()
    # Apprentissage de l'encodeur sur les étiquettes de classe concaténées
    le.fit(values)
    # Décode les étiquettes de classe encodées en utilisant l'encodeur inverse de LabelEncoder
    return(le.inverse_transform(y_guess_encoded)) # inverse_transform permet de récupérer les étiquettes de classe d'origine à partir des valeurs numériques encodées


### XGBOOST ###

def model_xgboost(x_train_encoded, y_train_encoded, x_test_encoded, guess=False, x_guess_encoded=None):
    """
    Cette fonction entraîne un modèle XGBoost en utilisant les données d'entraînement encodées et retourne les prédictions pour l'ensemble de test et l'ensemble de prédiction, ainsi que les probabilités de chaque prédiction si le paramètre "guess" est vrai.
    """
    # Création d'un objet de modèle XGBoost
    model = XGBClassifier()
    # Entraînement du modèle sur les données d'entraînement
    model.fit(x_train_encoded, y_train_encoded)
    # Prédiction des valeurs pour l'ensemble de test
    y_predict = model.predict(x_test_encoded)

    # Si une prédiction est demandée, prédiction des étiquettes pour l'ensemble de prédiction et calcul des probabilités pour chaque prédiction
    if guess:
        # Prédiction des étiquettes pour les données de prédiction encodées
        y_guess = model.predict(x_guess_encoded)
        # Prédiction des probabilités pour chaque étiquette pour les données de prédiction encodées
        proba_guess = model.predict_proba(x_guess_encoded)
    else:
        y_guess = None
        proba_guess = None

    return(y_predict, y_guess, proba_guess)


### RANDOM_FOREST_CLASSIFIER ###

def model_randomforest(x_train_encoded, y_train_encoded, x_test_encoded, guess=False, x_guess_encoded=None):
    """
    Cette fonction entraîne un modèle RandomForestClassifier à l'aide des données d'entraînement encodées et retourne les prédictions pour l'ensemble de test et l'ensemble de prédiction, ainsi que les probabilités de chaque prédiction.
    """
    # Définition du modèle RandomForestClassifier avec 100 estimateurs et une fonction de critère "gini" (choix courant)
    model = RandomForestClassifier(n_estimators=100, criterion='gini', random_state=0)

    # Entraînement du modèle sur les données d'entraînement encodées
    model.fit(x_train_encoded, y_train_encoded)

    # Prédiction des étiquettes pour l'ensemble de test
    y_predict = model.predict(x_test_encoded)

    # Si une prédiction est demandée, prédiction des étiquettes pour l'ensemble de prédiction et calcul des probabilités pour chaque prédiction
    if guess:
        # Prédiction des étiquettes pour les données de prédiction encodées
        y_guess = model.predict(x_guess_encoded)
        # Prédiction des probabilités pour chaque étiquette pour les données de prédiction encodées
        proba_guess = model.predict_proba(x_guess_encoded)
    else:
        y_guess = None
        proba_guess = None

    return(y_predict, y_guess, proba_guess)


### RESULTATS ###

def result(y_test_encoded, y_predict):
    """
    Cette fonction calcule la précision d'un modèle de classification et retourne le score et la matrice de confusion correspondante.
    """
    # Calcul de la précision du modèle
    R = accuracy_score(y_test_encoded, y_predict)
    # Calcul de la matrice de confusion
    mat = confusion_matrix(y_test_encoded, y_predict)
    return(f"Précis à {int(100*R)}%", mat)

