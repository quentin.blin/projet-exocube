'''Ce fichier contient une fonction "learn" qui permet d'entraîner un modèle de machine learning avec des données d'entraînement. Elle divise les données en ensembles d'entraînement et de test, encode les caractéristiques et les étiquettes, entraîne le modèle choisi (Random Forest ou XGBoost), prédit les étiquettes pour l'ensemble de test, et retourne le score de précision, la matrice de confusion, la prédiction des étiquettes pour les données de prédiction, et les probabilités de chaque prédiction.'''

from Learner.helpers_learn import *


def learn(model, guess=False, x_guess=None):
    """
    Cette fonction permet d'entraîner un modèle de Machine Learning avec les données de formation. Elle divise les données en ensembles d'entraînement et de test, encode les caractéristiques et les étiquettes, entraîne le modèle choisi, prédit les étiquettes pour l'ensemble de test et retourne le score de précision et la matrice de confusion.

    Args : 
    - model : 'randomforest' ou 'xgboost'
    - guess: variable booléenne pour indiquer si la fonction doit également prédire les étiquettes pour les données de prédiction
    - x_guess: un dataframe contenant les données de prédiction
    """
    # Chargement des données de formation depuis le fichier CSV
    trainingDf = import_csv("Learner/TrainingDB/trainingDB.csv")

    # Division des données en ensembles d'entraînement et de test en utilisant la fonction "split" définie dans helpers_learn.py
    x_train, y_train, x_test, y_test = split(trainingDf, 0.8) # choix de 80% arbitraire mais commun

    # Encodage des caractéristiques et des étiquettes avec les fonctions "features_encoder" et "label_encoder" définies dans helpers_learn.py
    x_train_encoded, x_test_encoded, x_guess_encoded = features_encoder(x_train, x_test, guess, x_guess)
    y_train_encoded, y_test_encoded = label_encoder(y_train, y_test)

    # Entraînement du modèle choisi et prédiction des étiquettes pour l'ensemble de test avec les fonctions "model_randomforest" et "model_xgboost" définies dans helpers_learn.py
    if model == 'randomforest':
        y_predict_encoded, y_guess_encoded, proba_guess = model_randomforest(x_train_encoded, y_train_encoded, x_test_encoded, guess, x_guess_encoded)
    elif model == 'xgboost':
        y_predict_encoded, y_guess_encoded, proba_guess = model_xgboost(x_train_encoded, y_train_encoded, x_test_encoded, guess, x_guess_encoded)
    else:
        raise Error("Le nom de l'algorithme fourni n'est pas correct.")

    # Calcul du score de précision et de la matrice de confusion avec la fonction "result" définie dans helpers_learn.py
    txt, mat = result(y_test_encoded, y_predict_encoded)

    # Décodage de l'étiquette prédite pour les données de prédiction avec la fonction "label_decoder" définie dans helpers_learn.py
    if guess:
        y_guess = int(label_decoder(y_train, y_test, y_guess_encoded))
    else:
        y_guess = None

    # Retour du score de précision, de la matrice de confusion, de la prédiction de l'étiquette à prédire et des probabilités de chaque prédiction
    return(txt, mat, y_guess, proba_guess)
