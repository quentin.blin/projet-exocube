## Ce code a pour but de générer les FFT de l'ensemble des fichiers csv à notre disposition et de les étiquetter.

import pandas as pd
import glob
from FFT import spectre
import csv
import numpy as np


noms = {} # dictionnaire qui contient l'ensemble des étiquettes attribuées


def name(liste): # fonction qui désigne l'étiquette à attribuer à un spectre
    nom = [0, 0, 0, 0, 0, 0, 0, 0]
    for i in range(len(liste)):
        if 1.0 in liste: # On place un 1 en 8ème position si le capteur d'id 1 est présent
            nom[7] = 1
        if 2.0 in liste: # idem
            nom[6] = 1
        if 3.0 in liste:
            nom[5] = 1
        if 4.0 in liste:
            nom[4] = 1
        if 5.0 in liste:
            nom[3] = 1
        if 6.0 in liste:
            nom[2] = 1
        if 7.0 in liste:
            nom[1] = 1
        if 8.0 in liste:
            nom[0] = 1
    nom = str(nom[0]) + str(nom[1]) + str(nom[2]) + str(nom[3]) + \
        str(nom[4]) + str(nom[5]) + str(nom[6]) + str(nom[7])
    names = noms.keys()
    if nom in names:
        end = int(noms[nom][-1][-2:])
        new = end+1
        if new < 10:
            end_2 = '0' + str(new)
        else:
            end_2 = new
        noms[nom].append(nom + '_' + str(end_2))
    else:
        noms[nom] = [nom + '_00'] # on ajoute un compteur à l'étiquette pour éviter les doublons de noms
    return noms[nom][-1]



"""Code à utiliser dans le cas où on utilise les fichiers csv initiaux"""

# chemin_dossier = "./dfincsv"
# fichiers_csv = glob.glob(chemin_dossier + "/*.csv")
# for i in range(len(fichiers_csv)):
#     with open(fichiers_csv[i], 'r') as fichier:
#         dialecte = csv.Sniffer().sniff(fichier.read(1024))
#     df = pd.read_csv(fichiers_csv[i], delimiter=dialecte.delimiter)
#     sensor_ids = df['SensorId'].unique()
#     sensor_ids = sensor_ids.tolist()
#     nom = name(sensor_ids)
#     try :
#         spectre(df, nom, './FFT')
#     except :
#         None


"""Code à utiliser si on fait de la data_augmentation"""

# # commenter une des 2 lignes suivantes
# # chemin_dossier = "./data-augmentee"
# chemin_dossier = "./data-augmentee2"

# fichiers_csv = glob.glob(chemin_dossier + "/*.csv")
# for i in range(len(fichiers_csv)):
#     with open(fichiers_csv[i], 'r') as fichier:
#         dialecte = csv.Sniffer().sniff(fichier.read(1024))
#     df = pd.read_csv(fichiers_csv[i], delimiter=dialecte.delimiter)
#     nom = fichiers_csv[i].split('/')
#     nom = nom[1][-15:][:-4]
#     try :
#         # commenter une des 2 lignes suivantes
#         # spectre(df, nom, './FFT_aug')
#         spectre(df, nom, './FFT_aug2')

#     except :
#         None
