'''Ce fichier contient plusieurs fonctions utilisées pour manipuler des fichiers pcap et effectuer des opérations d'extraction et de traitement des paquets. Les fonctions incluent le chargement d'un fichier pcap, la création d'un dictionnaire de balises, la recherche de balises dans une chaîne de caractères, le filtrage des paquets HTTP, la fusion des sessions montantes et descendantes, etc.'''

import pandas as pd
from scapy.all import *


def loadPcap(pcap_path):
    """
    Cette fonction prend en entrée le chemin vers un fichier pcap et retourne un objet Scapy PacketList contenant les paquets du fichier.
    """
    return(rdpcap(pcap_path))


def createTagsDictionnary():
    """
    Cette fonction lit le fichier sensors_tags.txt et retourne un dictionnaire dont les clés sont des tuples (nom, ID du capteur) et les valeurs sont des listes de balises associées à chaque capteur.
    """

    file = open('Builder/sensors_tags.txt', 'r')
    tags_dict = {}
    # Parcours du fichier
    for line in file.readlines():
        name, sensor_id, tags, _ = line.split(" : ")
    # Le nom et l'ID du capteur sont stockés dans un tuple en tant que clé
        tags_dict[(name, int(sensor_id))] = tags.split(" , ")
    return(tags_dict)


def findTags(string, tags_dict):
    """
    Cette fonction prend en entrée une chaîne de caractères et un dictionnaire de balises, et retourne l'ID du capteur associé à la première balise trouvée dans la chaîne, ou None si aucune balise n'a été trouvée. Il s'agit d'une recherche d'adresse MAC ici.
    """
    # Initialisation d'une liste pour stocker les IDs de capteurs trouvés
    ID_list = []
    for key, values in tags_dict.items():
        # Parcours de chaque balise associée à un capteur dans le dictionnaire
        for value in values:
            # Vérification de la présence de la balise dans la chaîne de caractères
            if value in string:
                # Si la balise est présente, l'ID du capteur est ajouté à la liste
                ID_list.append(key[1])

    # Vérification du nombre d'IDs trouvés
    if len(set(ID_list)) > 1:
        # Si plusieurs IDs ont été trouvés, une erreur est levée
        raise ValueError("Une frame contient plusieurs labels ! \n Le contenu de la frame en question est : " + string)

    # Retour de l'ID du capteur (premier de la liste), ou None si aucun ID n'a été trouvé
    return int(ID_list[0]) if ID_list else None


def filterHTTPPackets(packets):
    """
    Cette fonction filtre les paquets HTTP dans une liste de paquets Scapy (paquets utilisant le port 80).
    """
    # Filtrage des paquets ayant une couche IP et un port source ou destination égal à 80
    filtered_packets = packets.filter(lambda x: x.haslayer(IP) and (x[IP].dport == 80 or x[IP].sport == 80))

    return filtered_packets


def mergeUpDownSessions(df):
    """
    Change l'identifiant de session (A > B) et (B > A) en (A <> B) pour l'utilisation suivante :
    >>> packetsData = packetsDataExtractor(packets)
    >>> updatedpacketsData = mergeUpDownSessions(packetsData)
    Prend en entrée un dataframe pandas contenant les informations sur les paquets (doit contenir une colonne "Session" avec l'identifiant de session) et renvoie en sortie un dataframe avec les identifiants de session modifiés.
    """
    # Obtention des identifiants de session uniques
    sessions = df['Session'].unique()

    # Création d'un dictionnaire pour stocker les correspondances entre les anciens et les nouveaux identifiants de session
    dict = {}

    # Parcourir chaque identifiant de session unique
    for session in sessions:
        # Extraire les informations de session (protocole, source, signe et destination) de l'identifiant
        protocol, src, sign, dest = session.split(' ')

        # Créer un nouvel identifiant de session avec destination et source inversées (voir suite)
        new_id = protocol + ' ' + dest + ' <> ' + src

        # Si ce nouvel identifiant de session est déjà présent dans le dictionnaire (ajouté précédemment pour une autre session), on le conserve
        if new_id in dict.values():
            dict[session] = new_id
        # Sinon, on ajoute cette nouvelle valeur au dictionnaire pour la session considérée
        else:
            dict[session] = protocol + ' ' + src + ' <> ' + dest

    # Remplacer les anciens identifiants de session par les nouveaux dans la colonne "Session" du DataFrame
    df.Session = df.apply((lambda y: dict[y.Session]), axis=1) # axis=1 implique que le traitement a lieu sur les lignes

    return(df)

