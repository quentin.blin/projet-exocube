"""Ce module définit deux fonctions utilisées pour créer une base de données à partir de fichiers de capture de paquets PCAP."""

from Builder.helpers_db import *
from Builder.helpers_pcap import *
from Builder.helpers_extractor import *
import glob
import pandas as pd

# Le module glob est utilisé en Python pour trouver tous les chemins correspondants à un modèle de nom de fichier donné dans un répertoire.


def build(pcap_path, db_dest):
    """
    Prend en argument le chemin d'accès au fichier PCAP et le répertoire de destination pour la BDD résultante
    """

    # Extraction du nom du fichier à partir de son chemin d'accès (prise en compte des chemins Linux ET Windows)
    name = pcap_path.split(".")[0].split("/")[-1].split("\\")[-1]

    # Chargement du fichier PCAP et filtrage des paquets HTTP
    packets = loadPcap(pcap_path)
    httpPackets = filterHTTPPackets(packets)
    # Création d'un dictionnaire de tags et extraction des données de chaque paquet HTTP
    tagsDictionnary = createTagsDictionnary()
    packetsData = packetsDataExtractor(httpPackets, tagsDictionnary)
    # Fusion des sessions et extraction des données de chaque session
    mergedSessions = mergeUpDownSessions(packetsData)
    sessionsData = sessionsDataExtractor(mergedSessions)

    # Vérifier si le dossier existe déjà
    if not os.path.exists(db_dest):
        os.makedirs(db_dest)

    # Création de la BDD et enregistrement au format CSV dans le répertoire de destination
    db = buildDataBase(sessionsData)
    db.to_csv(f"{db_dest}{name}-iot-db.csv", index=False)

    # Retour de la valeur True pour indiquer que la fonction s'est exécutée avec succès
    return True


def merge():
    """
    Fusionne tous les fichiers CSV de base de données dans le répertoire "DB/" et enregistre le résultat dans
    un fichier CSV dans le répertoire "Learner/TrainingDB/"
    """

    # Liste de tous les fichiers CSV de base de données dans le répertoire "DB/"
    db_files = glob.glob("DB/*.csv")

    # Chargement de chaque fichier CSV de base de données dans un DataFrame Pandas
    frames = [pd.read_csv(db_file) for db_file in db_files]

    # Fusion des DataFrame en un seul DataFrame
    main_df = pd.concat(frames, ignore_index=True)

    # Enregistrement du DataFrame fusionné au format CSV dans le répertoire "Learner/TrainingDB/"
    main_df.to_csv(f"Learner/TrainingDB/trainingDB.csv", index=False)

    # Retour de la valeur True pour indiquer que la fonction s'est exécutée avec succès
    return True
