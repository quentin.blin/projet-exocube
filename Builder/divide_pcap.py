"""Ce fichier prend un fichier pcap en entrée, le divise en sous-fichiers contenant des captures sur 30 minutes et les enregistre dans des fichiers distincts."""

from helpers_pcap import loadPcap
from scapy.all import wrpcap
from datetime import datetime, timedelta
import sys
import os

if __name__ == '__main__':
    # Vérification du nombre d'arguments passés en ligne de commande
    if len(sys.argv) != 2:
        pcapFileName = str(input("Saisir un unique nom de fichier pcap à diviser : "))
    else:
        pcapFileName = (sys.argv[1])

    # Chargement du fichier pcap en mémoire
    packets = loadPcap(pcapFileName)

    # Initialisation des variables pour la découpe du fichier pcap
    start_time = datetime.fromtimestamp(int(packets[0].time))
    subfile_num = 0
    print(f"Création du sous-fichier n°{subfile_num}...")
    pcapFileNameShort = pcapFileName[0:len(pcapFileName)-5]
    subfile_name = f"Captures/{pcapFileNameShort}-{subfile_num}.pcap"

    # Création du répertoire "Captures" s'il n'existe pas
    if not os.path.exists("Captures"):
        os.makedirs("Captures")

    # Boucle pour découper le fichier pcap en sous-fichiers contenant des captures sur 30 minutes
    for pkt in packets:
        # Si le temps écoulé depuis le début du fichier est supérieur à 30 minutes
        if datetime.fromtimestamp(int(pkt.time)) > start_time + timedelta(minutes=30):
            # On incrémente le numéro de sous-fichier
            subfile_num += 1
            print(f"Création du sous-fichier n°{subfile_num}...")
            # On crée un nouveau nom de fichier pour le nouveau sous-fichier
            subfile_name = f"Captures/{pcapFileNameShort}-{subfile_num}.pcap"
            # On met à jour la variable "start_time" pour le nouveau sous-fichier
            start_time = datetime.fromtimestamp(int(pkt.time))

        # On ajoute le paquet courant au sous-fichier en cours
        wrpcap(subfile_name, pkt, append=True)

    print("Terminé !")
