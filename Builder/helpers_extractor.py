'''Ce fichier contient deux fonctions. La première fonction extrait les principales informations de chaque paquet dans un dictionnaire scapy.rdpcap et les retourne sous la forme d'un DataFrame pandas. La deuxième fonction prend en entrée les données de paquets sous forme de DataFrame pandas et agrège les données pour chaque session, renvoyant un nouveau DataFrame où chaque ligne correspond à une session.'''

import pandas as pd
from scapy.all import *
from Builder.helpers_pcap import findTags
import logging

# Eviter l'affichage de messages d'informations lors de l'exécution du script
logging.getLogger("scapy.runtime").setLevel(logging.ERROR)


def packetsDataExtractor(packets, tagsDictionnary):
    """
    Extrait les principales informations de chaque paquet : session_id, time_stamp, port_src, port_dst, length, sensor.
    Prend en entrée un dictionnaire scapy.rdpcap contenant les paquets à analyser, et renvoie en sortie dataframe pandas contenant une ligne par paquet, avec les informations extraites.
    """

    # Initialisation des listes pour stocker les informations extraites
    pktSessionID = []
    pktBytes = []
    pktTimestamp = []
    pktPortsrc = []
    pktPortdest = []
    pktSensorID = []

   # Récupération des sessions de paquets dans le dictionnaire
    sessions = packets.sessions()

    # Boucle pour traiter chaque session de paquets
    for session_id, packetList in sessions.items():
        # Boucle pour traiter chaque paquet de la session en cours
        for pkt in packetList:

            # if "Raw" in pkt:
            #     print(pkt[Raw].load)
            #     print("-------------------------------")

            # Extraction des informations globales
            pktSessionID.append(session_id)
            pktBytes.append(pkt.len)
            pktTime = pd.Timestamp(
                float(pkt.time), unit='s', tz='Europe/Paris')
            pktTimestamp.append(pktTime)

            # Extraction des informations de la couche TCP
            pktPortsrc.append(pkt[IP].sport)
            pktPortdest.append(pkt[IP].dport)

            # Extraction des informations de contenu
            if pkt.haslayer(Raw):
                pktSensorID.append(findTags(str(pkt[Raw].load), tagsDictionnary))
            else:
                pktSensorID.append(None)

    # Création de séries pandas à partir des listes d'informations extraites : une série panda est similaire à un tableau numpy unidimensionnel, mais ses éléments sont indexés par des étiquettes
    session = pd.Series(pktSessionID).astype(str)
    bytes = pd.Series(pktBytes).astype(int)
    timestamp = pd.to_datetime(pd.Series(pktTimestamp).astype(
        str),  errors='coerce')  # coerce = valeur NaN si erreur
    portSrc = pd.Series(pktPortsrc).astype(int)
    portDest = pd.Series(pktPortdest).astype(int)
    sensorID = pd.Series(pktSensorID).astype('str')

    # Création d'un dataframe pandas à partir des séries d'informations extraites
    packets_df = pd.DataFrame({"Session": session, "Bytes": bytes, "Time": timestamp,
                              "PortSrc": portSrc, "PortDest": portDest, "SensorID": sensorID})

    return (packets_df)


def sessionsDataExtractor(packetsData):
    """
    Cette fonction prend en entrée les données de paquets sous forme d'un dataframe pandas et retourne les données agrégées pour chaque session sous la forme d'un dataframe dont chaque ligne correspond à une session.
    """

    # Création d'une liste pour stocker les données agrégées pour chaque session
    data_sessions = []

    # Boucle pour agréger les données pour chaque session
    for sessionId in packetsData['Session'].unique():
        pkts = packetsData[packetsData['Session'] == sessionId]
        bytes = pkts['Bytes'].sum()
        numberOfPackets = pkts.shape[0]
        startTime = pkts['Time'].min()
        endTime = pkts['Time'].max()
        donglePort = pkts['PortSrc'].max()
        sensorID = pkts['SensorID'].min()
        data_sessions.append(
            [sessionId, bytes, numberOfPackets, startTime, endTime, donglePort, sensorID])

    # Création d'un dataframe à partir de la liste de données agrégées
    data_sessions_df = pd.DataFrame(data_sessions, columns=[
        'Session', 'Bytes', 'NumberOfPackets', 'StartTime', 'EndTime', 'DonglePort', 'SensorID'])

    return (data_sessions_df)
