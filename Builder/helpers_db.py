'''Ce fichier prend en entrée des données de sessions et construit une base de données (DataFrame) en fusionnant les sessions appartenant au même objet. La base de données contient des statistiques telles que la durée moyenne des sessions, la moyenne des octets par session, le temps moyen entre les sessions, la variance du temps entre les sessions, etc.'''

from datetime import datetime
import pandas as pd
import numpy as np

# import pickle as pkl


def calculate_entropy(values):
    """
    Cette fonction prend en entrée une série de valeurs et retourne l'entropie de ces valeurs.

    Entrée : values - une série de valeurs
    Sortie : entropy - l'entropie des valeurs
    """

    # f = open("test_main", "wb")
    # pkl.dump(values, f)
    # f.close()

    # Calcul de la fréquence de chaque valeur dans la série
    counts = np.bincount(values[1:])

    # Calcul de la probabilité de chaque valeur
    probabilities = counts / len(values[1:])

    # Remplacer les probabilités de zéro par 1
    probabilities[probabilities == 0] = 1
    print(probabilities)

    # Calcul de l'entropie
    entropy = -np.sum(probabilities * np.log2(probabilities))

    return entropy


def buildDataBase(sessionsData):
    """
    Cette fonction prend en entrée un dataframe contenant des données de session pour tous les capteurs (helpers_pcap.sessionsDataExtractor()) et retourne un dataframe contenant les statistiques agrégées pour chaque capteur.
    """

    # Création d'une liste pour stocker les données agrégées de chaque capteur
    data_sensors = []
    # Boucle pour agréger les données pour chaque capteur
    # la méthode .unique permet d'agréger les données pour chaque capteur de manière séparée
    for sensorID in sessionsData['SensorID'].unique():
        # Sélection des données pour le capteur en cours
        sensorData = sessionsData[sessionsData['SensorID'] == sensorID]

        # Calcul de la durée moyenne de chaque session et de la taille moyenne des paquets pour le capteur en cours
        # la méthode dt permet d'accéder aux propriétés de date et d'heure d'un objet DatetimeIndex
        sessionDurationMean = (
            sensorData['EndTime'] - sensorData['StartTime']).dt.total_seconds().mean()
        sessionBytesMean = sensorData['Bytes'].mean().astype(int)

        # Calcul du temps moyen entre deux sessions pour le capteur en cours, ainsi que de la variance de ce temps
        timeBetween2Sessions = sensorData['StartTime'].diff(
        ).dt.total_seconds()
        sleepTimeMean = timeBetween2Sessions.mean() if pd.notna(
            timeBetween2Sessions.mean()) else 1800  # ATTENTION : valeurs arbitraires à revoir
        sleepTimeVariance = timeBetween2Sessions.var() if pd.notna(
            timeBetween2Sessions.var()) else 0

        # Appel d'une fonction pour calculer l'entropie du temps de sommeil pour le capteur en cours
        sleepTimeEntropy = calculate_entropy(timeBetween2Sessions)

        # Ajout des données agrégées pour le capteur en cours à la liste de données
        data_sensors.append([sensorID, sessionDurationMean, sessionBytesMean,
                             sleepTimeMean, sleepTimeVariance, sleepTimeEntropy])

    # Création d'un dataframe à partir de la liste de données agrégées
    df_data_sensors = pd.DataFrame(data_sensors, columns=[
                                   'SensorID', 'SessionDurationMean', 'SessionBytesMean', 'SleepTimeMean', 'SleepTimeVariance', 'SleepTimeEntropy'])
    return df_data_sensors
