import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import os
import csv
import pickle

# spectrum_file = 'dftest_00056_20220528165309.csv'
# df = pd.read_csv(spectrum_file)

# convertir tous les pcap en dftest via script1A, les insérer dans le dossier dftests

# list_files = os.listdir('dfest')

df = pd.read_csv("./dfincsv/dftest_00006_20220527155309.csv")


def spectre(df, name, chemin):
    print("initiate")
    # df prend en entrée un fichier sortant de Script_1A_modif qui prend lui même un pcap
    # réadapter avec mongoDB
    M = df["StartTime"]
    M = [M[k][11:].split(":") for k in range(len(M))]
    Mp = []
    e0 = M[0]
    t0 = float(e0[0]) * 3600 + float(e0[1]) * 60 + float(e0[2][:-6])
    for e in M:
        Mp.append(float(e[0]) * 3600 + float(e[1]) * 60 + float(e[2][:-6]) - t0)
    T = df["Taille"]
    delta = np.inf  # intervalle le plus grand possible qui distingue tous les paquets
    for i in range(len(Mp) - 1):
        inter = Mp[i + 1] - Mp[i]
        if inter < delta:
            delta = inter
    delta = 0.1
    # On peut prendre un delta de 0.01s. intervalle mini de 0.019s
    # essai de sous echantillonage à 0.1s
    # 30min d'enregistrement. 2 min de traitement
    print("formation signal")
    Signal = []
    time_disc = int(Mp[-1] / delta) + 1
    print("time_disc=", time_disc)
    for i in range(len(Mp)):
        for k in range(time_disc):
            if Mp[i] > k * delta and Mp[i] < (k + 1) * delta:
                Signal.append(T[i])
            else:
                Signal.append(0)

    # Calcul de la FFT du signal
    print("calcul de la fft")
    print("len(Signal)=", len(Signal))
    fft = np.fft.fft(Signal)
    print(fft)
    print("len fft =", len(fft))
    print("calcul des amplitudes")
    fft = np.abs(fft)
    # 3 digits arbitrairement, affiner
    fft_light = np.around(
        fft, decimals=3
    )  # vérifier que malgré le round on n'a pas de 1.23000000
    formatted = ["{:.3e}".format(x) for x in fft_light]  # seulement 3 valeurs
    # print(fft_light)
    fft_light_sym = formatted[: len(fft_light) // 2]
    print("calcul des fréquences")
    N = len(fft)
    freqs = np.fft.fftfreq(N)
    mask = freqs >= 0  # Masque pour filtrer les fréquences positives
    freqs = freqs[mask]

    freq_ampli = []
    for i in range(len(fft_light_sym)):
        freq_ampli.append([freqs[i], fft_light_sym[i]])

    # Tracé du spectre de puissance
    # plt.figure(0)
    # plt.plot(fft_light_sym)
    # plt.figure(1)
    # plt.plot(freqs,fft_light_sym)
    # plt.show()
    # on désactive le show pour lancer des calculs pendant longtemps

    # enregistrement du spectre
    # np.savetxt(name + '_light_sym.txt', fft_light_sym)
    l = [freqs, fft_light_sym]
    f = open(chemin + "/" + name + ".pkl", "wb")
    pickle.dump(l, f)
    print("end")


# spectre(df, "essai", "./FFT")


def split_spectra(df):
    # Liste des valeurs uniques de SensorId
    sensor_ids = df["SensorId"].unique()
    list_of_dfs = []
    # Boucle pour itérer sur les différentes valeurs de SensorId
    for sensor_id in sensor_ids:
        # Extraire le sous-dataframe pour chaque SensorId
        df_sensor_id = df.groupby("SensorId").get_group(sensor_id).reset_index()
        list_of_dfs.append(df_sensor_id)
    # print(len(list_of_dfs))
    for i in range(1, len(list_of_dfs)):
        name = "spectrum_sensor_" + str(i)
        spectre(list_of_dfs[i], name)


def listdf(list_files):
    for dftest in list_files:
        split_spectra(dftest)


# faire un algo d'IA pour séparer les spectres.

# phase heuristique FFT inverse
