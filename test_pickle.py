import pickle
import numpy as np
import matplotlib.pyplot as plt


def fft_mat(path):
    with open(path, 'rb') as fichier:
        l = pickle.load(fichier)
    matrice = np.array([l[0], l[1]])
    return matrice


freqs, fft_light_sym = fft_mat("./train_aug2/00000010_00.pkl")
#freqs2,fft_light_sym2 = fft_mat("./train/11100111_03.pkl")
plt.figure(0)
plt.plot(freqs, fft_light_sym)
# plt.figure(1)
# plt.plot(freqs2,fft_light_sym2)
plt.show()
