## Ce code a pour but de prédire le type de capteur à partir des informations contenues dans le JSON échangé avec le dongle 

from gensim import matutils
import nltk
import numpy as np
import re
from gensim.models import Word2Vec
import nltk
from translate import Translator
import pandas as pd
import os
import re
import csv

langues = ["fr", "en"] # Choisir les langues 

"""On créé un dictionnaire manuellement (pouvant être completé) contenant en clé le type du capteur, et en valeur des champs lexicaux associés à ces types"""
dic_fr = {"luminosité": ["lumière", "lumiere", "lumières", "lumieres", "lux", "luminosité", "lumi", "éclairage", "éclairement", "lum", "lumo", "lumino", "lumin"],
          "température": ["chaleur", "kelvin", "chaud", "froid", "°C", "°F", "K", "°", "température", 'temperature', "Temperature", "Température"],
          "présence": ["Détecteur", "Detecteur", "Capteur", "Mouvement", "Présence", "presence", "Détection", "detection", "Infrarouge", "Ultrasons", "ultrason", "Mouvement", "Sensorielle"],
          "caméra": ["Caméra", "Objectif", "Capteur", "Image", "Vidéo", "Enregistrement", "Surveillance", "Vidéosurveillance", "Zoom", "Autofocus", "Obturateur", "ISO", "Résolution", "Pixel", "Webcam", "focus", "résolution"],
          "humidité": ["humidité", "Hygromètre", "Humidité relative", "Humidité absolue", "Condensation", "Humidificateur", "Déshumidificateur", "Ventilation", "point de rosée"],
          "fumée et monoxde de carbone": ["fumée", "combustion", "particules fines", "particules", "particule", "feu"],
          "ouverture": ["ouvrir", "déverouiller", "deverouiller", "fermer", "fermé", "ouvert", "Ouvert", "ouverture", "Ouverture", "OpenClose", "openclose"],
          "niveau d'eau": ["hauteur", "mètre", "mètres", "metre", "metres"],
          "gaz": ["PPM", "ppm", "PPB", "ppb", "µg/m3", "gaz"]}



def create_liste_mot(dict): # A partir du dictionnaire, construire une liste de mots en minuscule
    mots = []
    for cle in dict.keys():
        for value in dict[cle]:
            mots.append(value.lower())
    return mots


def translate_dict(dict, target_lang, source_lang): # Traduire les dictionnaires dans les langues choisies dans la liste langues 
    translator = Translator(to_lang=target_lang, from_lang=source_lang)
    translated_dict = {}
    for key in dict.keys():
        translated_values = []
        for value in dict[key]:
            translation = translator.translate(value)
            translated_values.append(translation)
        translated_dict[key] = translated_values
    return translated_dict


dictionnaires = [dic_fr]
for k in range(len(langues)-1):
    dictionnaires.append(translate_dict(dic_fr, langues[k+1], langues[0]))


def predict_type_sensor(f_json, dic, liste_mots, model, precision): # Prédire le type du capteur
    tokens = nltk.word_tokenize(f_json.lower()) # tokenisation du fichier json, découpage en une liste de mots
    mots_similaires = []
    for token in tokens:
        if token[0] == "'":
            token = token[1:]
        token = re.sub(r'\d+', '', token) # Supprimer les numéros contenus dans les tokens
        if token in liste_mots:
            mots_similaires.append(token) # si le token est dans le dictionnaire, on l'ajoute à la liste
            similarite_max = 1
        elif token in model.wv.key_to_index:
            similarite_max = 0
            mot_similaire = ''
            for mot in liste_mots:
                similarite = matutils.cossim(
                    np.array(model[token]), np.array(model[mot]))[0][0] # On calcule la similarité cosinus entre chaque token et les mots contenus dans la liste de mots 
                if similarite > similarite_max: # on désigne un seuil au dela duquel on conserve le token
                    similarite_max = similarite
                    mot_similaire = mot
            if similarite_max > precision: # On conserve uniquement un token, celui ayant la meilleure similarité cosinus 
                mots_similaires.append(mot_similaire)
    if len(mots_similaires) == 0:
        return None
    elif len(mots_similaires) != 0:
        for key in dic.keys():
            type = ""
            if mots_similaires[0] in dic[key]:
                type = key
                break
    return type # renvoie la clé correspondant au type de capteur 


def liste_pred(liste_jsons, liste_dic, precision): # construit la liste des prédictions qui contient la prédiction pour chaque langue, liste qui sera ajoutée au csv
    pred = []
    for dic in liste_dic:
        liste_mots = create_liste_mot(dic)
        model = Word2Vec([liste_mots], min_count=1)
        pred_inter = []
        for json in liste_jsons:
            chaine = str(json)
            pred_inter.append(predict_type_sensor(
                chaine, dic, liste_mots, model, precision))
        pred.append(pred_inter)
    pred_capteur = []
    for i in range(len(pred[0])):
        pred_capteur.append([])
    for i in range(len(pred)):
        for j in range(len(pred[0])):
            pred_capteur[j].append(pred[i][j])
    return pred_capteur


"""On construit ici les fichiers csv augmentés de la liste de prédiction"""
fich_json = os.listdir('./dfincsv') 
extension = ".csv"
fichiers_csv = [fichier for fichier in fich_json if fichier.endswith(extension)]
for fichier in fichiers_csv:
    with open("./dfincsv/"+ fichier, 'r') as f:
        dialecte = csv.Sniffer().sniff(f.read(1024))
    df = pd.read_csv("./dfincsv/" + fichier,delimiter=dialecte.delimiter)
    liste_jsons = df['Info']
    list_pred = liste_pred(liste_jsons, dictionnaires, 0.5)
    df['Prediction'] = list_pred
    df.to_csv("./data_json_predicted/" + fichier)