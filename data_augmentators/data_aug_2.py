## Même fonction que data_aug_1 mais cette fois-ci faite pour générer des fichiers csv ne contenant que les capteurs 2.0 et 3.0 (pour faire des tests sur notre IA)

import pandas as pd
import csv
import os
import random

noms = {}
nb_genere = 5


def name(liste):
    nom = [0, 0, 0, 0, 0, 0, 0, 0]
    for i in range(len(liste)):
        if 1.0 in liste:
            nom[7] = 1
        if 2.0 in liste:
            nom[6] = 1
        if 3.0 in liste:
            nom[5] = 1
        if 4.0 in liste:
            nom[4] = 1
        if 5.0 in liste:
            nom[3] = 1
        if 6.0 in liste:
            nom[2] = 1
        if 7.0 in liste:
            nom[1] = 1
        if 8.0 in liste:
            nom[0] = 1
    nom = str(nom[0]) + str(nom[1]) + str(nom[2]) + str(nom[3]) + \
        str(nom[4]) + str(nom[5]) + str(nom[6]) + str(nom[7])
    names = noms.keys()
    if nom in names:
        end = int(noms[nom][-1][-2:])
        new = end+1
        if new < 10:
            end_2 = '0' + str(new)
        else:
            end_2 = new
        noms[nom].append(nom + '_' + str(end_2))
    else:
        noms[nom] = [nom + '_00']
    return noms[nom][-1]


def data_augmentation2(filepath, path_to_new_dir):
    longueur = random.randint(1, 2)
    liste = random.sample(range(2, 4), min(3, longueur))
    liste_decimales = list(map(lambda x: x + 0.0, liste))
    df = pd.read_csv(filepath)
    sensor_id = df['SensorId']
    nom = name(liste_decimales)
    if liste_decimales[0] in sensor_id:
        df1 = df[df.SensorId == liste_decimales[0]]
    if (longueur > 1) and liste_decimales[0] not in sensor_id:
        df1 = df[df.SensorId == liste_decimales[1]]
    if longueur>1:
        df2 = df[df.SensorId == liste_decimales[1]]
        df1 = pd.concat([df1, df2])
    df1.to_csv(path_to_new_dir + '/' + nom +'.csv')


l = os.listdir('./dfincsv')

for i in range(3,len(l)):
    for k in range(nb_genere):
        data_augmentation2('./dfincsv/'+l[i], './data-augmentee2')