## Ce code a pour but de générer des nouveaux fichiers csv à partir des existants pour faire de la data_augmentation

import pandas as pd
import csv
import os
import random

noms = {} # dictionnaire qui stocke l'ensemble des étiquettes données aux fichiers csv
path = './dfincsv'
nb_genere = 10 # de chaque fichier csv existant, nous tirons nb_genere nouveaux fichiers csv  

l = os.listdir(path)

def name(liste): # Cette fonction détermine les étiquettes des fichiers csv 
    nom = [0, 0, 0, 0, 0, 0, 0, 0]
    for i in range(len(liste)):
        if 1.0 in liste: # On place un 1 en 8ème position si le capteur d'id 1 est présent
            nom[7] = 1
        if 2.0 in liste: # idem ...
            nom[6] = 1
        if 3.0 in liste:
            nom[5] = 1
        if 4.0 in liste:
            nom[4] = 1
        if 5.0 in liste:
            nom[3] = 1
        if 6.0 in liste:
            nom[2] = 1
        if 7.0 in liste:
            nom[1] = 1
        if 8.0 in liste:
            nom[0] = 1
    nom = str(nom[0]) + str(nom[1]) + str(nom[2]) + str(nom[3]) + \
        str(nom[4]) + str(nom[5]) + str(nom[6]) + str(nom[7]) # On réunit les différentes valeurs cntenues dans name pour former un str
    names = noms.keys()
    if nom in names: # on ajoute à l'étiquette un compteur pour éviter les doublous de nom
        end = int(noms[nom][-1][-2:])
        new = end+1
        if new < 10:
            end_2 = '0' + str(new)
        else:
            end_2 = new
        noms[nom].append(nom + '_' + str(end_2))
    else:
        noms[nom] = [nom + '_00']
    return noms[nom][-1]


def data_augmentation(filepath, path_to_new_dir, n): # Cette fonction génère les n nouveaux fichiers csv 
    for i in range(n):
        longueur = random.randint(1, 8)
        liste = random.sample(range(1, 9), min(9,longueur))
        liste_decimales = list(map(lambda x: x + 0.0, liste)) # Construire une liste avec 1.0 plutot que 1 (forme des valeurs contenues dans SensorId)
        df = pd.read_csv(filepath)
        nom = name(liste_decimales)
        df1 = df[df.SensorId == liste_decimales[0]]
        for i in range(1, len(liste_decimales)):
            try:
                df2 = df[df.SensorId == liste_decimales[i]]
                df1 = pd.concat([df1, df2])
            except:
                None
        df1.to_csv(path_to_new_dir + '/' + nom +'.csv')

for i in range(3,len(l)) :
    data_augmentation(path +'/'+ l[i], './data-augmentee',nb_genere) # enregistre les nouveaux fichiers dans data-augmentee 

