# Projet Exocube

## Table des matières

- [Présentation](#Présentation)
- [Contact](#contact)

## Présentation

Ce code est la composante principale du projet Défense & Sécurité, du pôle IoT, en partenariat avec l'Exocube.

Il s'agit de réaliser des captures de trafic de données sur un réseau contenant un ou plusieurs objets domotiques connectés afin d’entraîner un algorithme d’intelligence artificielle qui permettra à terme de reconnaître les objets présents sur un réseau à partir d’une capture de l’activité sur ce dernier. Pour cela, il est nécessaire d'élaborer une base de données suffisamment complète pour l'entraînement. Les applications visées sont diverses, en lien avec les intérêts du ministère de l’Armée. L’idée serait par exemple d'infiltrer un réseau domotique pour en identifier les appareils (lors d'une intervention de police criminelle par exemple), qui peuvent donner accès à de précieuses informations.

## Contact

Deux groupes ont travaillé sur ce projet. Voici le contact des dernières personnes à avoir travaillé dessus :
- quentin.blin@student-cs.fr
- hugues.du-moulinet-dhardemare@student-cs.fr
- francois.petit@student-cs.fr
- eugenie.patard@student-cs.fr
- hugo.nguyen@student-cs.fr
